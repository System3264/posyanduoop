/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.controllers;

import java.util.List;
import java.util.Observable;
import org.posyandu.daos.KaderDAO;
import org.posyandu.interfaces.KaderInterface;
import org.posyandu.models.Kader;
import org.posyandu.models.OperasiCRUD;
/**
 *
 * @author GenoSyst
 */
public class KaderController extends Observable{
    private KaderInterface dao = new KaderDAO();
    private OperasiCRUD crud;
    public void setDAO(KaderInterface k){
        dao = k;
    }
    
    public void setDml(Kader k, OperasiCRUD c){
        boolean hasil = false;
        this.crud = c;
        switch(c){
            case INSERT: hasil = dao.insert(k);
                break;
            case UPDATE: hasil = dao.update(k);
                break;
            case DELETE: hasil = dao.delete(k);
                break;
        }
        setChanged();
        if(hasil){
            notifyObservers(k);
        }else{
            notifyObservers();
        }
    }
    
    public OperasiCRUD getCrudState(){
        return crud;
    }
    
    public Kader loginKader(String user, String pass){
        return dao.login(user,pass);
    }
    
    public List<Kader> getBy_Id(String user){
        return dao.getByID(user);
    }
    
    public List<Kader> getAllKader(){
        return dao.getAllKader();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.controllers;

import java.util.List;
import java.util.Observable;
import org.posyandu.daos.TimbangDAO;
import org.posyandu.interfaces.TimbangInterface;
import org.posyandu.models.Timbang;
import org.posyandu.models.OperasiCRUD;
/**
 *
 * @author GenoSyst
 */
public class TimbangController extends Observable{
    private TimbangInterface dao = new TimbangDAO();
    private OperasiCRUD crud;
    public void setDAO(TimbangInterface t){
        dao = t;
    }
    
    public void setDml(Timbang t, OperasiCRUD c){
        boolean hasil = false;
        this.crud = c;
        switch(c){
            case INSERT: hasil = dao.insert(t);
                break;
            case UPDATE: hasil = dao.update(t);
                break;
            case DELETE: hasil = dao.delete(t);
                break;
        }
        setChanged();
        if(hasil){
            notifyObservers(t);
        }else{
            notifyObservers();
        }
    }
    
    public OperasiCRUD getCrudState(){
        return crud;
    }
    
    public List<Timbang> getById_anak(String id_anak){
        return dao.getById_Anak(id_anak);
    }
    
    public List<Timbang> getAllTimbang(){
        return dao.getAllTimbang();
    }
}

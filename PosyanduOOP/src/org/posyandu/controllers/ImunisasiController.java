/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.controllers;

import java.util.List;
import java.util.Observable;
import org.posyandu.daos.ImunisasiDAO;
import org.posyandu.interfaces.ImunisasiInterface;
import org.posyandu.models.Imunisasi;
import org.posyandu.models.OperasiCRUD;
/**
 *
 * @author GenoSyst
 */
public class ImunisasiController extends Observable{
    private ImunisasiInterface dao = new ImunisasiDAO();
    private OperasiCRUD crud;
    public void setDAO(ImunisasiInterface im){
        dao = im;
    }
    
    public void setDml(Imunisasi im, OperasiCRUD c){
        boolean hasil = false;
        this.crud = c;
        switch(c){
            case INSERT: hasil = dao.insert(im);
                break;
            case UPDATE: hasil = dao.update(im);
                break;
            case DELETE: hasil = dao.delete(im);
                break;
        }
        setChanged();
        if(hasil){
            notifyObservers(im);
        }else{
            notifyObservers();
        }
    }
    
    public OperasiCRUD getCrudState(){
        return crud;
    }
    
    public List<Imunisasi> getById_anak(String id_anak){
        return dao.getById_Anak(id_anak);
    }
    
    public List<Imunisasi> getAllImunisasi(){
        return dao.getAllImunisasi();
    }
}

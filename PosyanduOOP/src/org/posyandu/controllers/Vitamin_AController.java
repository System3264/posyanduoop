/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.controllers;

import java.util.List;
import java.util.Observable;
import org.posyandu.daos.Vitamin_ADAO;
import org.posyandu.interfaces.Vitamin_AInterface;
import org.posyandu.models.Vitamin_A;
import org.posyandu.models.OperasiCRUD;
/**
 *
 * @author GenoSyst
 */
public class Vitamin_AController extends Observable{
    private Vitamin_AInterface dao = new Vitamin_ADAO();
    private OperasiCRUD crud;
    public void setDAO(Vitamin_AInterface va){
        dao = va;
    }
    
    public void setDml(Vitamin_A va, OperasiCRUD c){
        boolean hasil = false;
        this.crud = c;
        switch(c){
            case INSERT: hasil = dao.insert(va);
                break;
            case UPDATE: hasil = dao.update(va);
                break;
            case DELETE: hasil = dao.delete(va);
                break;
        }
        setChanged();
        if(hasil){
            notifyObservers(va);
        }else{
            notifyObservers();
        }
    }
    
    public OperasiCRUD getCrudState(){
        return crud;
    }
    
    public List<Vitamin_A> getById_anak(String id_anak){
        return dao.getById_Anak(id_anak);
    }
    
    public List<Vitamin_A> getAllVitamin_A(){
        return dao.getAllVitamin_A();
    }
}

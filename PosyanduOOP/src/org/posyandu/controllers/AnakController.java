/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.controllers;

import java.util.List;
import java.util.Observable;
import org.posyandu.daos.AnakDAO;
import org.posyandu.interfaces.AnakInterface;
import org.posyandu.models.Anak;
import org.posyandu.models.OperasiCRUD;
/**
 *
 * @author GenoSyst
 */
public class AnakController extends Observable{
    private AnakInterface dao = new AnakDAO();
    private OperasiCRUD crud;
    public void setDAO(AnakInterface a){
        dao = a;
    }
    
    public void setDml(Anak a, OperasiCRUD c){
        boolean hasil = false;
        this.crud = c;
        switch(c){
            case INSERT: hasil = dao.insert(a);
                break;
            case UPDATE: hasil = dao.update(a);
                break;
            case DELETE: hasil = dao.delete(a);
                break;
        }
        setChanged();
        if(hasil){
            notifyObservers(a);
        }else{
            notifyObservers();
        }
    }
    
    public OperasiCRUD getCrudState(){
        return crud;
    }
    
    public List<Anak> getAllAnak(){
        return dao.getAllAnak();
    }
    
    public Anak getById(String kodeAnak){
        return dao.getByID(kodeAnak);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.models;

import java.time.LocalDate;
import java.util.Date;
/**
 *
 * @author GenoSyst
 */
public class Vitamin_A {
    private String id_imvita;
    private String id_anak;
    private String id_kader;
    private int umurAnak;
    private Date tglImun;
    private String ket;
    
    public Vitamin_A(String id_imvita,String id_anak,String id_kader,int umurAnak,Date tglImun,String ket){
        this.id_imvita = id_imvita;
        this.id_anak = id_anak;
        this.id_kader = id_kader;
        this.umurAnak = umurAnak;
        this.tglImun = tglImun;
        this.ket = ket;
    }
    
    public Vitamin_A(String id_imvita){
        this.id_imvita = id_imvita;
    }
    
    public String getId_Imvita(){
        return id_imvita;
    }
    
    public void setId_Imvita(String id_imvita){
        this.id_imvita = id_imvita;
    }
    
    public String getId_Anak(){
        return id_anak;
    }
    
    public void setId_Anak(String id_anak){
        this.id_anak = id_anak;
    }
    
    public String getId_Kader(){
        return id_kader;
    }
    
    public void setId_Kader(String id_kader){
        this.id_kader = id_kader;
    }
    
    public int getUmurAnak(){
        return umurAnak;
    }
    
    public void setUmurAnak(int umurAnak){
        this.umurAnak = umurAnak;
    }
    
    public Date getTglImun(){
        return tglImun;
    }
    
    public void settglImun(Date tglImun){
        this.tglImun = tglImun;
    }
    
    public String getKet(){
        return ket;
    }
    
    public void setKet(String ket){
        this.ket = ket;
    }
    
    @Override
    public String toString(){
        return this.id_imvita;
    }
}

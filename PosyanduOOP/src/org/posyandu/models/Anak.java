/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.models;

import java.time.LocalDate;
import java.util.Date;
/**
 *
 * @author GenoSyst
 */
public class Anak {
    private String kodeAnak;
    private String namaAnak;
    private LocalDate tglLahir;
    private String jenisKelamin;
    private String alamat;
    private float tinggiAnak;
    private float beratAnak;
    private String namaIbu;
    private String namaAyah;
    private String stat;
    
    public Anak(String kodeAnak,String namaAnak,LocalDate tglLahir,String jenisKelamin,String alamat,
        float tinggiAnak, float beratAnak, String namaIbu, String namaAyah,String stat){
        this.kodeAnak = kodeAnak;
        this.namaAnak = namaAnak;
        this.tglLahir = tglLahir;
        this.jenisKelamin = jenisKelamin;
        this.alamat = alamat;
        this.tinggiAnak = tinggiAnak;
        this.beratAnak = beratAnak;
        this.namaIbu = namaIbu;
        this.namaAyah = namaAyah;
        this.stat = stat;
    }
    
    public Anak(String kodeAnak){
        this.kodeAnak = kodeAnak;
    }
    
    public String getKodeAnak(){
        return kodeAnak;
    }
    
    public void setKodeAnak(String kodeAnak){
        this.kodeAnak = kodeAnak;
    }
    
    public String getNamaAnak(){
        return namaAnak;
    }
    
    public void setNamaAnak(String namaAnak){
        this.namaAnak = namaAnak;
    }
    
    public LocalDate getTglLahir(){
        return tglLahir;
    }
    
    public void setTglLahir(LocalDate tglLahir){
        this.tglLahir = tglLahir;
    }
    
    public String getJenisKelamin(){
        return jenisKelamin;
    }
    
    public void setJenisKelamin(String jenisKelamin){
        this.jenisKelamin = jenisKelamin;
    }
    
    public String getAlamat(){
        return alamat;
    }
    
    public void setAlamat(String alamat){
        this.alamat = alamat;
    }
    
    public float getTinggiAnak(){
        return tinggiAnak;
    }
    
    public void setTinggiAnak(float tinggiAnak){
        this.tinggiAnak = tinggiAnak;
    }
    
    public float getBeratAnak(){
        return beratAnak;
    }
    
    public void setBeratAnak(float beratAnak){
        this.beratAnak = beratAnak;
    }
    
    public String getNamaIbu(){
        return namaIbu;
    }
    
    public void setNamaIbu(String namaIbu){
        this.namaIbu = namaIbu;
    }
    
    public String getNamaAyah(){
        return namaAyah;
    }
    
    public void setNamaAyah(String namaAyah){
        this.namaAyah = namaAyah;
    }
    
    public String getStat(){
        return stat;
    }
    
    public void setStat(String stat){
        this.stat = stat;
    }
    
    @Override
    public String toString(){
        return this.namaAnak;
    }
}

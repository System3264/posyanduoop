/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.models;

import java.time.LocalDate;
import java.util.Date;
/**
 *
 * @author GenoSyst
 */
public class Timbang {
    private String id_timbang;
    private String id_anak;
    private String id_kader;
    private int umurAnak;
    private float beratAnak;
    private float beratTimbang;
    private String statB;
    private float tinggiAnak;
    private float tinggiUkur;
    private String statT;
    private Date tglTimbang;
    
    public Timbang(String id_timbang,String id_anak,String id_kader,int umurAnak,float beratAnak, float beratTimbang,
    String statB,float tinggiAnak, float tinggiUkur,String statT,Date tglTimbang){
        this.id_timbang = id_timbang;
        this.id_anak = id_anak;
        this.id_kader = id_kader;
        this.umurAnak = umurAnak;
        this.beratAnak = beratAnak;
        this.beratTimbang = beratTimbang;
        this.statB = statB;
        this.tinggiAnak = tinggiAnak;
        this.tinggiUkur = tinggiUkur;
        this.statT = statT;
        this.tglTimbang = tglTimbang;
    }
    
    public Timbang(String id_timbang){
        this.id_timbang = id_timbang;
    }

    public String getId_Timbang(){
        return id_timbang;
    }
    
    public void setId_Timbang(String id_timbang){
        this.id_timbang = id_timbang;
    }
    
    public String getId_Anak(){
        return id_anak;
    }
    
    public void setId_Anak(String id_anak){
        this.id_anak = id_anak;
    }
    
    public String getId_Kader(){
        return id_kader;
    }
    
    public void setId_Kader(String id_kader){
        this.id_kader = id_kader;
    }
    
    public int getUmurAnak(){
        return umurAnak;
    }
    
    public void setUmurAnak(int umurAnak){
        this.umurAnak = umurAnak;
    }
    public float getBeratAnak(){
        return beratAnak;
    }
    
    public void setBeratAnak(float beratAnak){
        this.beratAnak = beratAnak;
    }
    
    public float getBeratTimbang(){
        return beratTimbang;
    }
    
    public void setBeratTimbang(float beratTimbang){
        this.beratTimbang = beratTimbang;
    }
    
    public String getStatB(){
        return statB;
    }
    
    public void setStatB(String statB){
        this.statB = statB;
    }
    
    public float getTinggiAnak(){
        return tinggiAnak;
    }
    
    public void setTinggiAnak(Float tinggiAnak){
        this.tinggiAnak = tinggiAnak;
    }
    
    public float getTinggiUkur(){
        return tinggiUkur;
    }
    
    public void setTinggiUkur(float tinggiUkur){
        this.tinggiUkur = tinggiUkur;
    }
    
    public String getStatT(){
        return statT;
    }
    
    public void setStatT(String statT){
        this.statT = statT;
    }
    
    public Date getTglTimbang(){
        return tglTimbang;
    }
    
    public void setTglTimbang(Date tglTimbang){
        this.tglTimbang = tglTimbang;
    }
    
    @Override
    public String toString(){
        return this.id_timbang;
    }
}

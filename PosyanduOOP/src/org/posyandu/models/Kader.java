/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.models;

import java.time.LocalDate;
import java.util.Date;
/**
 *
 * @author GenoSyst
 */
public class Kader {
    private String id_kader;
    private String pass;
    private String namaKader;
    private LocalDate tglLahir;
    private String alamat;
    private String no_tlp;
    private String jenisKelamin;
    private String role;
    
    public Kader(String id_kader,String pass,String namaKader,LocalDate tglLahir,String alamat
            ,String no_tlp,String jenisKelamin,String role){
        this.id_kader = id_kader;
        this.pass = pass;
        this.namaKader = namaKader;
        this.tglLahir = tglLahir;
        this.alamat = alamat;
        this.no_tlp = no_tlp;
        this.jenisKelamin = jenisKelamin;
        this.role = role;
    }
    
    public Kader(String id_kader){
        this.id_kader = id_kader;
    }

    
    public String getId_Kader(){
        return id_kader;
    }
    
    public void setId_Kader(String id_kader){
        this.id_kader = id_kader;
    }
    
    public String getPass(){
        return pass;
    }
    
    public void setPass(String pass){
        this.pass = pass;
    }
    
    public String getNamaKader(){
        return namaKader;
    }
    
    public void setNamaKader(String namaKader){
        this.namaKader = namaKader;
    }
    
    public LocalDate getTglLahir(){
        return tglLahir;
    }
    
    public void setTglLahir(LocalDate tglLahir){
        this.tglLahir = tglLahir;
    }
    
    public String getAlamat(){
        return alamat;
    }
    
    public void setAlamat(String alamat){
        this.alamat = alamat;
    }
    
    public String getNo_Tlp(){
        return no_tlp;
    }
    
    public void setNo_Tlp(String no_tlp){
        this.no_tlp = no_tlp;
    }
    
    public String getJenisKelamin(){
        return jenisKelamin;
    }
    
    public void setJenisKelamin(String jenisKelamin){
        this.jenisKelamin = jenisKelamin;
    }
    
    public String getRole(){
        return role;
    }
    
    public void setRole(String role){
        this.role = role;
    }
    
    @Override
    public String toString(){
        return this.id_kader;
    }
}

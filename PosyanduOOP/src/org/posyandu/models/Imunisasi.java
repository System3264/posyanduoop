/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.models;

import java.time.LocalDate;
import java.util.Date;
/**
 *
 * @author GenoSyst
 */
public class Imunisasi {
    private String id_imun;
    private String id_anak;
    private String id_kader;
    private int umurAnak;
    private String jenisImun;
    private Date tglImun;
    
    public Imunisasi(String id_imun,String id_anak,String id_kader,int umurAnak,String jenisImun,Date tglImun){
        this.id_imun = id_imun;
        this.id_anak = id_anak;
        this.id_kader = id_kader;
        this.umurAnak = umurAnak;
        this.tglImun = tglImun;
        this.jenisImun = jenisImun;
    }
    
    public Imunisasi(String id_imun){
        this.id_imun = id_imun;
    }
    
    public String getId_Imun(){
        return id_imun;
    }
    
    public void setId_Imun(String id_imun){
        this.id_imun = id_imun;
    }
    
    public String getId_Anak(){
        return id_anak;
    }
    
    public void setId_Anak(String id_anak){
        this.id_anak = id_anak;
    }
    
    public String getId_Kader(){
        return id_kader;
    }
    
    public void setId_Kader(String id_kader){
        this.id_kader = id_kader;
    }
    
    public int getUmurAnak(){
        return umurAnak;
    }
    
    public void setUmurAnak(int umurAnak){
        this.umurAnak = umurAnak;
    }
    
    public String getJenisImun(){
        return jenisImun;
    }
    
    public void setJenisImun(String jenisImun){
        this.jenisImun = jenisImun;
    }
    
    public Date getTglImun(){
        return tglImun;
    }
    
    public void setTglImun(Date tglImun){
        this.tglImun = tglImun;
    }
    
    @Override
    public String toString(){
        return this.id_imun;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.interfaces;

import java.util.List;
import org.posyandu.models.Kader;

/**
 *
 * @author GenoSyst
 */
public interface KaderInterface {
    public boolean insert(Kader k);
    public boolean update(Kader k);
    public boolean delete(Kader k);
    public Kader login(String id_kader,String pass);// yg ini belom tau mau gimana sistem e
    
    public List<Kader> getAllKader();
    public List<Kader> getByID(String id_kader);
    public List<Kader> getByName(String namaKader);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.interfaces;

import java.util.List;
import org.posyandu.models.Timbang;

/**
 *
 * @author GenoSyst
 */
public interface TimbangInterface {
    public boolean insert(Timbang t);
    public boolean update(Timbang t);
    public boolean delete(Timbang t);
    
    public List<Timbang> getAllTimbang();
    public Timbang getByID(String id_timbang);
    public List<Timbang> getById_Anak(String id_anak);
    //public Anak getNumID(String kodeAnak);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.interfaces;

import java.util.List;
import org.posyandu.models.Anak;

/**
 *
 * @author GenoSyst
 */
public interface AnakInterface {
    public boolean insert(Anak a);
    public boolean update(Anak a);
    public boolean delete(Anak a);
    
    public List<Anak> getAllAnak();
    public Anak getByID(String kodeAnak);
    public List<Anak> getByName(String namaAnak);
    //public Anak getNumID(String kodeAnak);
}

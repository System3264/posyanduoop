/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.interfaces;

import java.util.List;
import org.posyandu.models.Imunisasi;

/**
 *
 * @author GenoSyst
 */
public interface ImunisasiInterface {
    public boolean insert(Imunisasi im);
    public boolean update(Imunisasi im);
    public boolean delete(Imunisasi im);
    
    public List<Imunisasi> getAllImunisasi();
    public Imunisasi getByID(String id_imun);
    public List<Imunisasi> getById_Anak(String id_anak);
    //public Anak getNumID(String kodeAnak);
}

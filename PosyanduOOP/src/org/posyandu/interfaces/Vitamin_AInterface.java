/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.interfaces;

import java.util.List;
import org.posyandu.models.Vitamin_A;

/**
 *
 * @author GenoSyst
 */
public interface Vitamin_AInterface {
    public boolean insert(Vitamin_A va);
    public boolean update(Vitamin_A va);
    public boolean delete(Vitamin_A va);
    
    public List<Vitamin_A> getAllVitamin_A();
    public Vitamin_A getByID(String id_imvita);
    public List<Vitamin_A> getById_Anak(String id_anak);
    //public Anak getNumID(String kodeAnak);
}

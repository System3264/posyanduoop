/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.daos;

import org.posyandu.interfaces.Vitamin_AInterface;
import org.posyandu.models.Vitamin_A;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author GenoSyst
 */
public class Vitamin_ADAO implements Vitamin_AInterface{
    @Override
    public boolean insert(Vitamin_A va){
        String sql="INSERT INTO tvit_a values (?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
            statement.setString(1, va.getId_Imvita());
            statement.setString(2, va.getId_Anak());
            statement.setString(3, va.getId_Kader());
            statement.setInt(4, va.getUmurAnak());
            statement.setString(5, null);
            statement.setString(6, va.getKet());
            
            
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
        }
        catch(Exception e){
            Logger.getLogger(Vitamin_A.class.getName()).log(Level.SEVERE,null,e);
        }
        return false;
        
    }
    @Override
        public boolean update(Vitamin_A va){
            String sql = "UPDATE tanak SET ket=? WHERE id_imvita =?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, va.getKet());
                statement.setString(2, va.getId_Imvita());
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Vitamin_A.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
    @Override
        public boolean delete(Vitamin_A va){
            String sql = "DELETE FROM tjabatan WHERE kodejab=?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, va.getId_Imvita());
                int row = statement.executeUpdate();
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Vitamin_A.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
        
    @Override
        public Vitamin_A getByID(String id_imvita){
            throw new UnsupportedOperationException("Not Supported yet.");
        }
        
    @Override
        public List<Vitamin_A> getById_Anak(String id_anak){
            List<Vitamin_A> vitas = new ArrayList<Vitamin_A>();
            String sql = "SELECT * FROM tvit_a WHERE id_anak = ? ";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    statement.setString(1, id_anak);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Vitamin_A va = new Vitamin_A(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getInt(4),
                                rs.getDate(5),
                                rs.getString(6)
                                );
                        vitas.add(va);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Vitamin_A.class.getName()).log(Level.SEVERE,null,e);
            }
            return vitas;
        }
    @Override
        public List<Vitamin_A> getAllVitamin_A(){
            List<Vitamin_A> vitas = new ArrayList<Vitamin_A>();
            String sql = "SELECT * FROM tvit_a";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Vitamin_A va = new Vitamin_A(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getInt(4),
                                rs.getDate(5),
                                rs.getString(6)
                                );
                        vitas.add(va);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Vitamin_A.class.getName()).log(Level.SEVERE,null,e);
            }
            return vitas;
        }
    
    public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }
}

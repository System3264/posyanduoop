/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.daos;

import org.posyandu.interfaces.KaderInterface;
import org.posyandu.models.Kader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.time.LocalDate;
/**
 *
 * @author GenoSyst
 */
public class KaderDAO implements KaderInterface{
    @Override
    public boolean insert(Kader k){
        String sql="INSERT INTO tkader values (?, ?, ?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
            statement.setString(1, k.getId_Kader());
            statement.setString(2, k.getPass());
            statement.setString(3, k.getNamaKader());
            statement.setDate(4, Date.valueOf(k.getTglLahir()));
            statement.setString(5, k.getAlamat());
            statement.setString(6, k.getNo_Tlp());
            statement.setString(7, k.getJenisKelamin());
            statement.setString(8, "0");
            
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
        }
        catch(Exception e){
            Logger.getLogger(Kader.class.getName()).log(Level.SEVERE,null,e);
        }
        return false;
        
    }
    @Override
        public boolean update(Kader k){
            String sql = "UPDATE tkader SET namaKader = ?, tgl_lahir = ?, gender = ?, no_tlp = ?, alamat = ?, role = ? WHERE id_kader =?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, k.getNamaKader());
                statement.setDate(2, Date.valueOf(k.getTglLahir()));
                statement.setString(5, k.getAlamat());
                statement.setString(4, k.getNo_Tlp());
                statement.setString(3, k.getJenisKelamin());
                statement.setString(6, k.getRole());
                statement.setString(7, k.getId_Kader());
                
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Kader.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
    @Override
        public boolean delete(Kader k){
            String sql = "DELETE FROM tkader WHERE id_kader=?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, k.getId_Kader());
                int row = statement.executeUpdate();
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Kader.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
    @Override
        public Kader login(String id_kader,String pass){
            Kader kaders = null;
            try{
                    if(Koneksi.bukaKoneksi() == null){
                        return null;
                    }else{
                    String sql = "SELECT * FROM tkader WHERE id_kader = ? AND pass = ?";
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    statement.setString(1, id_kader);
                    statement.setString(2, pass);
                    ResultSet rs = statement.executeQuery();
                    if(rs.next()){
                        Kader k = new Kader(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                convertToLocalDateViaSqlDate(rs.getDate(4)),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7),
                                rs.getString(8));
                                kaders = k;
                        }
                    statement.close();
                    }
                }
            catch(Exception e){
                Logger.getLogger(Kader.class.getName()).log(Level.SEVERE,null,e);
            }
            return kaders;
        }
        
    @Override
        public List<Kader> getByID(String id_kader){
            List<Kader> kaders = new ArrayList<Kader>();
            try{
                    if(Koneksi.bukaKoneksi() == null){
                        return null;
                    }else{
                    String sql = "SELECT * FROM tkader WHERE id_kader = ?";
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    statement.setString(1, id_kader);
                    ResultSet rs = statement.executeQuery();
                    if(rs.next()){
                        Kader k = new Kader(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                convertToLocalDateViaSqlDate(rs.getDate(4)),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7),
                                rs.getString(8));
                                kaders.add(k);
                        }
                    statement.close();
                    }
                }
            catch(Exception e){
                Logger.getLogger(Kader.class.getName()).log(Level.SEVERE,null,e);
            }
            return kaders;
        }
        
    @Override
        public List<Kader> getByName(String namaKader){
            List<Kader> kaders = new ArrayList<Kader>();
            String sql = "SELECT * FROM tkader where namaKader = ?";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    statement.setString(1, namaKader);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Kader k = new Kader(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                convertToLocalDateViaSqlDate(rs.getDate(4)),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7),
                                rs.getString(8));
                        kaders.add(k);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Kader.class.getName()).log(Level.SEVERE,null,e);
            }
            return kaders;
        }
        
    @Override
        public List<Kader> getAllKader(){
            List<Kader> kaders = new ArrayList<Kader>();
            String sql = "SELECT * FROM tkader";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Kader k = new Kader(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                convertToLocalDateViaSqlDate(rs.getDate(4)),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getString(7),
                                rs.getString(8));
                        kaders.add(k);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Kader.class.getName()).log(Level.SEVERE,null,e);
            }
            return kaders;
        }
        
    public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }
}


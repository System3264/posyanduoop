/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.daos;

import java.sql.*;
/**
 *
 * @author GenoSyst
 */
public class Koneksi {
    private static Connection konn;
    private static final String URL = "jdbc:mysql://localhost/dbposyandu"; ///locationnya nanti disesuain sama db yang dibuat
    private static final String DRIVENAME = "com.mysql.jdbc.Driver";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    
    public static Connection bukaKoneksi(){
        if (konn == null){
            try{
                try{
                    Class.forName(DRIVENAME);
                konn = (com.mysql.jdbc.Connection) DriverManager.getConnection(URL,USERNAME,PASSWORD);  
                }
                catch(SQLException sqle){
                    System.out.println("Driver not found: "+ sqle);
                }
            }
            catch(Exception e){
                System.out.println("Driver not found: "+ e);
            }
        }
        return konn;
    }
    
    public static Connection tutupKoneksi(){
        if (konn != null){
            try{
                konn.close();
            }catch(SQLException e){
                System.out.println("Error: "+e);
            }
        }
        return konn;
    }
    
    public static void main(String args[]){
        new Koneksi();
        try{
            bukaKoneksi();
            System.out.println("Buka Koneksi Berhasil");
        }
        catch(Exception ee){
            
        }
    }

    public static Connection getConnection() {
        Connection connection = null;
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/dbposyandu"; //ganti dengan database mu
        String user = "root";
        String password = "";
        if (connection == null) {
            try {
                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, password);
            } catch (ClassNotFoundException | SQLException error) {
               System.exit(0);
            }

        }
        return connection;
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.daos;

import org.posyandu.interfaces.TimbangInterface;
import org.posyandu.models.Timbang;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author GenoSyst
 */
public class TimbangDAO implements TimbangInterface{
    @Override
    public boolean insert(Timbang t){
        String sql="INSERT INTO ttimbang values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
            statement.setString(1, t.getId_Timbang());
            statement.setString(2, t.getId_Anak());
            statement.setString(3, t.getId_Kader());
            statement.setFloat(4, t.getUmurAnak());
            statement.setFloat(5, t.getBeratAnak());
            statement.setFloat(6, t.getBeratTimbang());
            statement.setString(7, t.getStatB());
            statement.setFloat(8, t.getTinggiAnak());
            statement.setFloat(9, t.getTinggiUkur());
            statement.setString(10, t.getStatT());
            statement.setString(11, null);
            
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
        }
        catch(Exception e){
            Logger.getLogger(Timbang.class.getName()).log(Level.SEVERE,null,e);
        }
        return false;
        
    }
    @Override
        public boolean update(Timbang t){
            String sql = "UPDATE tanak SET namaAnak=? WHERE kodeAnak =?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, t.getId_Anak());
                statement.setString(2, t.getId_Anak());
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Timbang.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
    @Override
        public boolean delete(Timbang t){
            String sql = "DELETE FROM tjabatan WHERE kodejab=?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, t.getId_Timbang());
                int row = statement.executeUpdate();
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Timbang.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
        
    @Override
        public Timbang getByID(String id_timbang){
            throw new UnsupportedOperationException("Not Supported yet.");
        }
        
    @Override
        public List<Timbang> getById_Anak(String id_anak){
            List<Timbang> timbangs = new ArrayList<Timbang>();
            String sql = "SELECT * FROM ttimbang WHERE id_anak = ? ";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    statement.setString(1, id_anak);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Timbang t = new Timbang(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getInt(4),
                                rs.getFloat(5),
                                rs.getFloat(6),
                                rs.getString(7),
                                rs.getFloat(8),
                                rs.getFloat(9),
                                rs.getString(10),
                                rs.getDate(11)
                                );
                        System.out.println( t.getId_Timbang()+" "+t.getId_Anak()+" "+t.getId_Kader()+" "+t.getUmurAnak()
                            +" "+t.getBeratAnak()+" "+t.getBeratTimbang()+" "+t.getTinggiAnak()+" "+t.getTinggiUkur());
                        timbangs.add(t);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Timbang.class.getName()).log(Level.SEVERE,null,e);
            }
            return timbangs;
        }
    @Override
        public List<Timbang> getAllTimbang(){
            List<Timbang> timbangs = new ArrayList<Timbang>();
            String sql = "SELECT * FROM ttimbang";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Timbang t = new Timbang(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getInt(4),
                                rs.getFloat(5),
                                rs.getFloat(6),
                                rs.getString(7),
                                rs.getFloat(8),
                                rs.getFloat(9),
                                rs.getString(10),
                                rs.getDate(11)
                                );
                        timbangs.add(t);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Timbang.class.getName()).log(Level.SEVERE,null,e);
            }
            return timbangs;
        }
    
    public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }
}

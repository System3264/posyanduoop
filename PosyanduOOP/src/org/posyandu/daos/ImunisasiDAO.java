/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.daos;

import org.posyandu.interfaces.ImunisasiInterface;
import org.posyandu.models.Imunisasi;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author GenoSyst
 */
public class ImunisasiDAO implements ImunisasiInterface{
    @Override
    public boolean insert(Imunisasi im){
        String sql="INSERT INTO timun values (?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
            statement.setString(1, im.getId_Imun());
            statement.setString(2, im.getId_Anak());
            statement.setString(3, im.getId_Kader());
            statement.setInt(4, im.getUmurAnak());
            statement.setString(5, im.getJenisImun());
            statement.setString(6, null);
            
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
        }
        catch(Exception e){
            Logger.getLogger(Imunisasi.class.getName()).log(Level.SEVERE,null,e);
        }
        return false;
        
    }
    @Override
        public boolean update(Imunisasi im){
            String sql = "UPDATE timun SET jenisImun = ? WHERE id_imun =?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, im.getJenisImun());
                statement.setString(2, im.getId_Imun());
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Imunisasi.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
    @Override
        public boolean delete(Imunisasi im){
            String sql = "DELETE FROM timun WHERE id_imun=?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, im.getId_Imun());
                int row = statement.executeUpdate();
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Imunisasi.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
        
    @Override
        public Imunisasi getByID(String id_imun){
            throw new UnsupportedOperationException("Not Supported yet.");
        }
        
    @Override
        public List<Imunisasi> getById_Anak(String id_anak){
            List<Imunisasi> imuns = new ArrayList<Imunisasi>();
            String sql = "SELECT * FROM timun WHERE id_anak = ? ";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    statement.setString(1, id_anak);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Imunisasi im = new Imunisasi(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getInt(4),
                                rs.getString(5),
                                rs.getDate(6)
                                );
                        imuns.add(im);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Imunisasi.class.getName()).log(Level.SEVERE,null,e);
            }
            return imuns;
        }
    @Override
        public List<Imunisasi> getAllImunisasi(){
            List<Imunisasi> imuns = new ArrayList<Imunisasi>();
            String sql = "SELECT * FROM timun";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Imunisasi im = new Imunisasi(rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getInt(4),
                                rs.getString(5),
                                rs.getDate(6)
                                );
                        imuns.add(im);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Imunisasi.class.getName()).log(Level.SEVERE,null,e);
            }
            return imuns;
        }
    
    public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }
}

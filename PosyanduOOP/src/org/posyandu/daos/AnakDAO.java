/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.posyandu.daos;

import org.posyandu.interfaces.AnakInterface;
import org.posyandu.models.Anak;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author GenoSyst
 */
public class AnakDAO implements AnakInterface{
    @Override
    public boolean insert(Anak a){
        String sql="INSERT INTO tanak values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try{
            PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
            statement.setString(1, a.getKodeAnak());
            statement.setString(2, a.getNamaAnak());
            statement.setDate(3, Date.valueOf(a.getTglLahir()));
            statement.setString(4, a.getJenisKelamin());
            statement.setString(5, a.getAlamat());
            statement.setFloat(6, a.getTinggiAnak());
            statement.setFloat(7, a.getBeratAnak());
            statement.setString(8, a.getNamaIbu());
            statement.setString(9, a.getNamaAyah());
            statement.setString(10, "0");
            
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
        }
        catch(Exception e){
            Logger.getLogger(Anak.class.getName()).log(Level.SEVERE,null,e);
        }
        return false;
        
    }
    @Override
        public boolean update(Anak a){
            String sql = "UPDATE tanak SET namaAnak=?, tgl_lahir=?, gender=?, alamat=?, tinggiBdn=?, beratBdn=?, namaIbu=?, namaAyah=?, status=? WHERE id_anak =?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, a.getNamaAnak());
                statement.setDate(2, Date.valueOf(a.getTglLahir()));
                statement.setString(3, a.getJenisKelamin());
                statement.setString(4, a.getAlamat());
                statement.setFloat(5, a.getTinggiAnak());
                statement.setFloat(6, a.getBeratAnak());
                statement.setString(7, a.getNamaIbu());
                statement.setString(8, a.getNamaAyah());
                statement.setString(9, a.getStat());
                statement.setString(10, a.getKodeAnak());
            int row = statement.executeUpdate();
            if(row > 0){
                return true;
            }
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Anak.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
    @Override
        public boolean delete(Anak a){
            String sql = "DELETE FROM tanak WHERE id_anak=?";
            try{
                PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                statement.setString(1, a.getKodeAnak());
                int row = statement.executeUpdate();
            statement.close();
            }
            catch(Exception e){
                Logger.getLogger(Anak.class.getName()).log(Level.SEVERE,null,e);
            }
            return false;
        }
        
    @Override
        public Anak getByID(String kodeAnak){
            Anak anaks = null;
            String sql = "SELECT * FROM tanak WHERE id_anak = ? ";
            try{
                    if(Koneksi.bukaKoneksi() == null){
                        return null;
                    }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    statement.setString(1, kodeAnak);
                    ResultSet rs = statement.executeQuery();
                    if(rs.next()){
                        Anak a = new Anak(rs.getString(1),
                                rs.getString(2),
                                convertToLocalDateViaSqlDate(rs.getDate(3)),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getFloat(6),
                                rs.getFloat(7),
                                rs.getString(8),
                                rs.getString(9),
                                rs.getString(10));
                        anaks = a;
                        }
                    statement.close();
                    }
                }
            catch(Exception e){
                Logger.getLogger(Anak.class.getName()).log(Level.SEVERE,null,e);
            }
            return anaks;
        }
        
    @Override
        public List<Anak> getByName(String namaAnak){
            throw new UnsupportedOperationException("Not Supported yet.");
        }
    @Override
        public List<Anak> getAllAnak(){
            List<Anak> anaks = new ArrayList<Anak>();
            String sql = "SELECT * FROM tanak";
            try{
                if(Koneksi.bukaKoneksi() == null){
                    return null;
                }else{
                    PreparedStatement statement = Koneksi.bukaKoneksi().prepareStatement(sql);
                    ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        Anak a = new Anak(rs.getString(1),
                                rs.getString(2),
                                convertToLocalDateViaSqlDate(rs.getDate(3)),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getFloat(6),
                                rs.getFloat(7),
                                rs.getString(8),
                                rs.getString(9),
                                rs.getString(10));
                        anaks.add(a);
                    }
                    statement.close();
                }
            }
            catch(Exception e){
                Logger.getLogger(Anak.class.getName()).log(Level.SEVERE,null,e);
            }
            return anaks;
        }
    
    public LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }
}

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12 Jan 2020 pada 13.21
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbposyandu`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tanak`
--

CREATE TABLE `tanak` (
  `id_anak` char(6) NOT NULL,
  `namaAnak` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `gender` char(1) NOT NULL,
  `alamat` text NOT NULL,
  `tinggiBdn` float NOT NULL,
  `beratBdn` float NOT NULL,
  `namaIbu` varchar(30) NOT NULL,
  `namaAyah` varchar(30) DEFAULT NULL,
  `status` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tanak`
--

INSERT INTO `tanak` (`id_anak`, `namaAnak`, `tgl_lahir`, `gender`, `alamat`, `tinggiBdn`, `beratBdn`, `namaIbu`, `namaAyah`, `status`) VALUES
('qww1w', '2wee', '1212-12-12', 'P', '121222', 121, 122, 'sdasddd', 'asdsdddd', '0'),
('qwww', 'wee', '1212-12-12', 'L', '121222', 121, 122, 'sdasddd', 'asdsdddd', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `timun`
--

CREATE TABLE `timun` (
  `id_imun` char(8) NOT NULL,
  `id_anak` char(6) NOT NULL,
  `id_kader` char(8) NOT NULL,
  `umur_anak_bln` int(11) NOT NULL,
  `jenis_imun` varchar(30) NOT NULL,
  `tgl_imun` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tkader`
--

CREATE TABLE `tkader` (
  `id_kader` char(8) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `namaKader` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `no_tlp` char(12) NOT NULL,
  `gender` char(1) NOT NULL,
  `role` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tkader`
--

INSERT INTO `tkader` (`id_kader`, `pass`, `namaKader`, `tgl_lahir`, `alamat`, `no_tlp`, `gender`, `role`) VALUES
('12121212', '12121212', '12312322', '1122-11-11', '1211212222', '12332222', 'L', '0'),
('www', 'qwqweee', 'qweqweqwe', '1212-12-12', '12321332', '123213333', 'P', '0'),
('www11', 'qwqweee', 'qweqweqwe', '1212-12-12', '12321332', '123213333', 'P', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttimbang`
--

CREATE TABLE `ttimbang` (
  `id_timbang` char(8) NOT NULL,
  `id_anak` char(6) NOT NULL,
  `id_kader` char(8) NOT NULL,
  `umur_anak_bln` int(11) NOT NULL,
  `beratAwal` float NOT NULL,
  `beratTimbang` float NOT NULL,
  `beratStatus` char(1) NOT NULL DEFAULT '0',
  `tinggiAwal` float NOT NULL,
  `tinggiUkur` float NOT NULL,
  `tinggiStatus` char(1) NOT NULL DEFAULT '0',
  `tanggalTimbang` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tvit_a`
--

CREATE TABLE `tvit_a` (
  `id_imvita` char(8) NOT NULL,
  `id_anak` char(6) NOT NULL,
  `id_kader` char(8) NOT NULL,
  `tgl_beri_vita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tanak`
--
ALTER TABLE `tanak`
  ADD PRIMARY KEY (`id_anak`);

--
-- Indexes for table `timun`
--
ALTER TABLE `timun`
  ADD PRIMARY KEY (`id_imun`);

--
-- Indexes for table `tkader`
--
ALTER TABLE `tkader`
  ADD PRIMARY KEY (`id_kader`);

--
-- Indexes for table `ttimbang`
--
ALTER TABLE `ttimbang`
  ADD PRIMARY KEY (`id_timbang`);

--
-- Indexes for table `tvit_a`
--
ALTER TABLE `tvit_a`
  ADD PRIMARY KEY (`id_imvita`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
